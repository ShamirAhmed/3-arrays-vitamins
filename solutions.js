const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


function availableItems(items){
    return items.filter(item => item['available'] === true);
}

function vitaminCItemsOnly(items){
    return items.filter(item => item['contains'] === 'Vitamin C')
}

function vitaminAItems(items){
    return items.filter(item => item['contains'].indexOf('Vitamin A') !== -1)
}

function vitaminsBasedItems(items){
    return items.reduce((acc,curr) => {
        curr['contains'].split(', ').filter(vitamin => {
            if(acc[vitamin] === undefined)
                acc[vitamin] = [];
            acc[vitamin].push(curr['name']);
            return true;
        });
        return acc;
    }, {})
}

function numberOfVitaminsItemContains(items){
    return items.sort((item1, item2) => item2['contains'].split(',').length - item1['contains'].split(',').length);
}

